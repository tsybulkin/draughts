#
# Draughts
#
# State functions
#

from moves import get_killed, killed_by_king, get_jumps, get_all_moves


def get_legal_actions(State):
	# check mandatory jumps
	# if there are jumps return them
	# if there is no manaatory action, find legal slides
	
	Jumps = get_jumps(State)
	if len(Jumps) == 0: 
		return get_all_moves(State)
	else:
		return Jumps



def update_state(State, Action):
	#print "State:",State
	#print "Action:",Action

	N,Who,Ws,Bs,E,Wk,Bk = State
	if Who == 'w':
		Men1 = Ws[:]
		Kings1 = Wk[:]
		Men2 = Bs[:]
		Kings2 = Bk[:]
		Turn = 'b'
	else:
		Men1 = Bs[:]
		Kings1 = Bk[:]
		Men2 = Ws[:]
		Kings2 = Wk[:]
		Turn = 'w'
		N += 1

	Es = E[:]

	if Action[1] == '->':
		if Action[0] in Kings1:
			Kings1.remove(Action[0])
			Kings1.append(Action[2])
		else:	
			Men1.remove(Action[0])
			Men1.append(Action[2])

		Es.remove(Action[2])
		Es.append(Action[0])
		
	else: # it is a jump
		
		while len(Action) != 1:
			if Action[0] in Kings1:
				Killed = killed_by_king(Action[0],Action[2],Es)
				if Killed in Kings2:
					Kings2.remove(Killed)
				else:
					Men2.remove(Killed)

				Kings1.remove(Action[0])
				Kings1.append(Action[2])

			else:
				Killed = get_killed(Action[0],Action[2])
				if Killed in Kings2:
					Kings2.remove(Killed)
				else:
					Men2.remove(Killed)
				Men1.remove(Action[0])
				Men1.append(Action[2])
				
			Es.remove(Action[2])
			Es.append(Action[0])
			Es.append(Killed)
			Action = Action[2:]

	## check for new Kings
	if Who == 'w':
		for j in range(4):
			if (0,j) in Men1:
				Men1.remove((0,j))
				Kings1.append((0,j))
	else:
		for j in range(4):
			if (7,j) in Men1:
				Men1.remove((7,j))
				Kings1.append((7,j))

	#print "Next State:\nTurn: %s" % Turn, \
	#	"\nWhite men:",Ws, "\nBlack men:",Bs, "\nEmpty places:",Es,"\nKings:",Wk,Bk
	if len(Men1)+len(Men2)+len(Es)+len(Kings1)+len(Kings2) != 32: raise
	#print "Previous State:", State
	if Who == 'w':
		return (N,Turn,Men1,Men2,Es,Kings1,Kings2)
	else:
		return (N,Turn,Men2,Men1,Es,Kings2,Kings1)

