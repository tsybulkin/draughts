#
# Draughts game
#
# Agents module
# 
# Here described different agents that take State and legal moves as an input
# and returns the best move
#
from state import update_state, get_legal_actions
from random import random
from time import time


class Agent:
	def __init__(self, Name):
		self.name = Name
		

def choose_manually(State):
	T1 = time()
	Actions = get_legal_actions(State)
	for i in range(len(Actions)): 
		print "%i:" %i, 
		for s in Actions[i]: print str(s),
		print ""
	
	Msg = "Choose 0 - %i\n" % (len(Actions)-1)
	Choise = raw_input(Msg)
	if Choise.isdigit():
		N = int(Choise)
		if N in range(len(Actions)): 
			return (Actions[N],time()-T1)

	print "You chose incorrect number"
	return (choose_manually(State), time()-T1)


def choose_randomly(State):
	T1 = time()
	Actions = get_legal_actions(State)
	return (Actions[ int(len(Actions)*random()) ], time() - T1)



def minimax2(State):
	"""
	consider his action and an action of his opponent, then estimate the position
	do analysis until next move is not a jump
	returns a better minimax move
	"""
	T1 = time()
	Depth = 2.5
	Actions = get_legal_actions(State)
	#for i in range(len(Actions)): print "%i:" %i, Actions[i]
	if len(Actions) == 1: return (Actions[0],time()-T1)

	(Who,_,_,_,_,_) = State
	if Who == 'w':
		(Estimation, Move) = get_max(State,Depth)
	else:
		(Estimation, Move) = get_min(State,Depth)

	print "MiniMax estimation:", Estimation, "Move:",Move
	return (Move, time() - T1)



def get_max(State,Depth):
	"""
	returns the best move and its Estimation (Est, Move) from the State
	"""
	Actions = get_legal_actions(State)
	if Actions == []: return (-999,None)
		
	if Depth <= 0 and Actions[0][1] == '->':
		return (get_simple_estimation(State),None)

	Variants = []
	for A in Actions:
		State1 = update_state(State,A)
		(Est,_) = get_min(State1,Depth-0.5)
		Variants.append((Est,A))

	if Variants == []: return (-999, None)

	Variants.sort()
	Variants.reverse()

	(Best_est,_) = Variants[0]
	Best_vars = [Variants[i] for i in range(len(Variants)) if Variants[i][0] == Best_est]

	return Best_vars[ int(random()*len(Best_vars)) ]


def get_min(State,Depth):
	"""
	returns the worst move and its Estimation (Est, Move) from the State
	"""
	Actions = get_legal_actions(State)
	if Actions == []: return (999,None)
		
	if Depth <= 0 and Actions[0][1] == '->':
		return (get_simple_estimation(State),None)

	Variants = []
	for A in Actions:
		State1 = update_state(State,A)
		(Est,_) = get_max(State1,Depth-0.5)
		Variants.append((Est,A))

	if Variants == []: return (999, None)

	Variants.sort()
	
	(Worst_est,_) = Variants[0]
	Worst_vars = [Variants[i] for i in range(len(Variants)) if Variants[i][0] == Worst_est]

	return Worst_vars[ int(random()*len(Worst_vars)) ]


def get_simple_estimation(State):
	(_,Ws,Bs,Es,Wk,Bk) = State

	return len(Ws)-len(Bs) + 5 * (len(Wk)-len(Bk))


