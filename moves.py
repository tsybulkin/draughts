#
# draughts
# module: moves
#
#

def get_all_moves(State):
	(_,Who,Ws,Bs,Es,Wk,Bk) = State
	Moves = []
	
	if Who == 'w': 
		for Man in Ws:
			New = get_place(Man,'dl')
			if New in Es: Moves.append( (Man,'->',New) )
			New = get_place(Man,'dr')
			if New in Es: Moves.append( (Man,'->',New) )

		for K in Wk:
			for Dir in get_directions():
				New = get_place(K,Dir)
				while New in Es:
					Moves.append( (K,'->',New) )
					New = get_place(New,Dir)

		
	else: 
		for Man in Bs:
			New = get_place(Man,'ul')
			if New in Es: Moves.append( (Man,'->',New) )
			New = get_place(Man,'ur')
			if New in Es: Moves.append( (Man,'->',New) )

		for K in Bk:
			for Dir in get_directions():
				New = get_place(K,Dir)
				while New in Es:
					Moves.append( (K,'->',New) )
					New = get_place(New,Dir)



	return Moves




def get_jumps(State):
	(_,Who,Ws,Bs,Es,Wk,Bk) = State
	Jumps = []
	
	if Who == 'w':
		for Piece in Ws:
			Jumps += find_jumps(None,Piece,Bs+Bk,Es,get_directions())
		for K in Wk:
			Jumps += find_king_jumps(K,Bs+Bk,Es)
	else:
		for Piece in Bs:
			Jumps += find_jumps(None,Piece,Ws+Wk,Es,get_directions())
		for K in Bk:
			Jumps += find_king_jumps(K,Ws+Wk,Es)

	return Jumps


def get_place(Place,Dir):
	if Dir == 'ul': return left_top(Place)
	elif Dir == 'ur': return right_top(Place)
	elif Dir == 'dl': return left_bot(Place)
	elif Dir == 'dr': return right_bot(Place)
	else:
		print "Wrong Place or Dir:", Place, Dir 
		raise


def get_directions(): return ['ul','ur','dl','dr']

def right(Dir):
	if Dir == 'ul': return 'ur'
	elif Dir == 'ur': return 'dr'
	elif Dir == 'dr': return 'dl'
	elif Dir == 'dl': return 'ul'
	else: raise


def left(Dir): return opposite(right(Dir))


def opposite(Dir):
	if Dir == 'ul': return 'dr'
	elif Dir == 'ur': return 'dl'
	elif Dir == 'dr': return 'ul'
	elif Dir == 'dl': return 'ur'
	else: raise


def find_jumps(Path,Place,Men2,Es,Directions):
	"""
	finds all extra jumps started from the end of the Path
	returns list of updated paths
	"""
	#print "Path:",Path, "Men2:",Men2

	New_paths = []
	#print "Finding jumps for:", (i,j)
	for Dir in Directions:	
		Killed = get_place(Place,Dir)
		Place_to_jump = get_place(Killed,Dir)
		#print "Place to jump:",Place_to_jump,"over:",Killed

		if (Killed in Men2) and (Place_to_jump in Es): 
			if Path != None:
				P = Path + (Place , 'x')
			else:
				P = (Place , 'x')
			New_men2 = Men2[:]
			New_men2.remove(Killed)
			New_Es = Es[:]
			New_Es.remove(Place_to_jump)
			New_Es.append(Killed)
			New_Es.append(Place)
			New_dirs = get_directions()
			New_dirs.remove(opposite(Dir))

			New_paths += find_jumps( P, Place_to_jump, New_men2, New_Es, New_dirs )		
	
	if len(New_paths) == 0 and Path == None : 
		return []
	elif len(New_paths) == 0:
		Path +=(Place,)
		return (Path),
	else:
		return New_paths



def find_king_jumps(K,Men2,Es):
	Single_jumps = []

	for Dir in get_directions():
		Killed = check_jump(K,Men2,Es,Dir)
		if Killed != None: Single_jumps.append((Killed,Dir))

	if Single_jumps == []:
		return []
	else:
		Path = (K,'x')
		Moves = []
		for (Killed,Dir) in Single_jumps:
			M = Men2[:]
			M.remove(Killed)
			E = Es[:]
			E.append(Killed)
			Moves += get_next_jumps(Path,Killed,M,E,Dir)
		return list(set(Moves))

		
def  check_jump(K,Men2,Es,Dir):
	"""
	Checks whether King <K> can kill anybody from <Men2> in the direction Directions
	returns Killed or None
	"""
	Killed = get_place(K,Dir)
	while Killed in Es:
		Killed = get_place(Killed,Dir)

	if Killed in Men2 and get_place(Killed,Dir) in Es: return Killed
	else: return None



def get_next_jumps(Path,Killed,Men2,Es,Dir):
	"""
	find all variants of jumping starting from Path and beating <Killed> in the direction <Dir>
	returns list of possible moves in the format [ ('P1','x',P2, ...), ... ]
	"""
	# find all starting points
	Start_points = [get_place(Killed,Dir)]
	P = get_place(Killed,Dir)
	while P in Es:
		Start_points.append(P)
		P = get_place(P,Dir)

	
	Single_jumps = []
	Directions = get_directions()
	Directions.remove(opposite(Dir))
	for P in Start_points:
		for Dir in Directions:
			K = check_jump(P,Men2,Es,Dir)
			if K != None and not((K,Dir) in Single_jumps): 
				Single_jumps.append((P,K,Dir))

	if Single_jumps == []:
		return [ Path + (P,) for P in Start_points]
	else:
		Moves = []
		for (P,Kil,Dir) in Single_jumps:
			New_path = Path + (P,'x')
			M = Men2[:]
			M.remove(Kil)
			E = Es[:]
			E.append(Kil)
			Moves += get_next_jumps(New_path,Kil,M,E,Dir)
		
		return Moves

		


def left_bot((i,j)):
	if i % 2 == 0:	return (i-1,j-1)
	else: 			return (i-1,j)

def left_top((i,j)):
	if i % 2 == 0:	return (i+1,j-1)
	else: 			return (i+1,j)

def right_bot((i,j)):
	if i % 2 == 0:	return (i-1,j)
	else: 			return (i-1,j+1)

def right_top((i,j)):
	if i % 2 == 0:	return (i+1,j)
	else: 			return (i+1,j+1)




def get_killed((i,j),(k,m)):
	if i % 2 == 0:
		return (i+k)/2,min(j,m)
	else:
		return (i+k)/2,max(j,m)



def killed_by_king((i,j),(k,m),Es):
	if k>i:
		if m>j:
			Dir = 'ur'
		else:
			Dir = 'ul'
	else:
		if m>j:
			Dir = 'dr'
		else:
			Dir = 'dl'

	Killed = get_place((i,j),Dir)
	while Killed != (k,m):
		if Killed in Es: 
			Killed = get_place(Killed,Dir)
			continue
		else:
			return Killed

	raise





