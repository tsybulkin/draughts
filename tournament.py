#
# Draughts game
# started September 2014
#

from draughts import main
from random import random



def tournament(Agents):
	"""
	Conducts a tournament among a given list of Agents and
	returns a list of Agents sorted by their place in the tornament
	"""
	if len(Agents)%2 != 0: 
		print "It must be even number og players in tournament"
		raise

	N = int(log(len(Agents),2)+1)

	Table = [ (random(), 0,i) for i in range(len(Agents)) ]
	Table.sort()
	Table = [ (0,i) for (_,0,i) in Table ]

	while N > 0:
		for i in range(0,len(Table),2):
			wScore,White = Table[i]
			bScore,Black = Table[i+1]
			(P,Q,_) = main(Agents[White],Agents[Black],0)
			Table[i] = (wScore+P,White)
			Table[i+1]=(bScore+Q,Black)

		Table.sort()
		N = N-1

	Table.reverse()
	return [Agents[i] for i in [ Index for (Score,Index) in Table]]






