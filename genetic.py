#
# Draughts game
# started September 2014
#

import sys
from random import random
from smart import GAgent
from draughts import run_game



def genetic(N,K):
	if N%2 != 0:
		print "Number of agents must be even"
		raise

	agents = {'w':GAgent('White'), 'b':GAgent('Black')}
	run_game(agents, 60, 10)
	#Agents = initialize_agents(N)





def initialize_agents(N):
	"""
	initialize N 'smart' agents with different DNA
	"""
	Agents = []
	for i in range(N):
		Agt = GAgent("Smart_"+str(i))
		Agents.append(Agt)




if __name__ == '__main__':
	A = sys.argv
	
	if len(A) != 3: 
		print "Usage: python genetic.py <N_agents> <K_generations>"
	else:
		genetic(int(A[1]),int(A[2]))
