#
# Draughts game
# started September 2014
#

import sys
import os.path

from debut import read_debut_data



def main(debut_moves_file):
	Debut_moves = read_debut_data(debut_moves_file)

	Keys = Debut_moves.keys()
	First_moves = set([ M1 for (M1,_) in Keys])
	Responds = set([M2 for (_,M2) in Keys])
	Best_moves = []

	for Move in First_moves:
		Best_responds = [ (Debut_moves.get((Move,Resp),(0,0))[1],Resp) for Resp in Responds]
		Best_responds.sort()

		Rate,Best_resp = Best_responds[0]
		Best_moves.append((Rate,Move,Best_resp))

	Best_moves.sort()
	Best_moves.reverse()
	print "Best moves for whites and responds for blacks:"

	for (R,M1,M2) in Best_moves:
		print "%s, %s Rate: %.2f" %(M1,M2,R)



if __name__ == '__main__':
	Args = sys.argv
	
	if len(Args) != 2: 
		print "Usage: python debut_moves.py <data_file>"
	else: 
		main(Args[1])

