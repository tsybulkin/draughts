# README #
## Draughts


Version 1.0

### Draughts (Checkers) game. Various agents can play against each other. Current version supports human, random, and egghead (simple minimax) agents ###

* Quick summary
* Version 1.0

### How do I get set up? ###

* A game uses Python 2.7. 
* Configuration
* Software has no dependancies 

### How to run draughts ###

python draughts.py <agent1> <agent2>

Available agents:
* random
* human
* egghead



### Who do I talk to? ###

* Repo owner or admin: Ian Tsybulkin
* ian.tsybulkin@gmail.com