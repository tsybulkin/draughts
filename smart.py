#
# Draughts game
#
# Agents module
# 
# Here described different agents that take State and legal moves as an input
# and returns the best move
#
from state import update_state, get_legal_actions
from random import random
from time import time

class GAgent:
	
	def __init__(self,Name):
		self.name = Name
		self.move1 = [1,3,4,5][(int(random()*4))]
		self.move2 = [int(random()*7) for i in range(7)]

		self.depth = 1.5+random()
		self.D1 = 0.05 + 0.45*random() # [0.05 .. 0.5]
		self.D2 = 0.2 + 0.3*random()  # [0.1 .. 0.5]
		self.D3 = 0.45 + 0.2*random()

		self.king2man_own = 3 + 5*random()
		self.king2man_opp = 3 + 5*random()
		self.next_king = 0.2 + 0.7*random() # [0.2 .. 0.9]

		self.eps = 0.2*random() # [0 .. 0.2]

		self.attack = 0.1
		self.defense = 0.1

	
	def mutate(self):
		return self


	def get_move(self,State):
		"""
		consider his action and an action of his opponent, then estimate the position
		do analysis until next move is not a jump
		returns a better minimax move
		"""
		T1 = time()
		(N,Who,w_man,_,_,_,_) = State
		
		if N==1 and Who =='w':
			return (self.get_white_first_move(), time()-T1)
		elif N==0 and Who == 'b':
			return (self.get_black_first_move(w_man), time()-T1) 

		Depth = self.depth
		Actions = get_legal_actions(State)
		#for i in range(len(Actions)): print "%i:" %i, Actions[i]
		if len(Actions) == 1: return (Actions[0],time()-T1)

		if Who == 'w':
			(Estimation, Move) = self.get_max(State,Depth)
		else:
			(Estimation, Move) = self.get_min(State,Depth)

		#print "EggHead estimation:", Estimation, "Move:",Move
		return (Move,time()-T1)



	def get_max(self,State,Depth):
		"""
		returns the best move and its Estimation (Est, Move) from the State
		"""
		Actions = get_legal_actions(State)
		if Actions == []: return (-999,None)
			
		if Depth <= 0 and Actions[0][1] == '->':
			return (self.get_estimation(State),None)

		Variants = []
		if len(Actions) <= 2: D = self.D1
		elif len(Actions) <= 4: D = self.D2
		#elif len(Actions) > 10: D = 1
		else: D = self.D3

		for A in Actions:
			(Est,_) = self.get_min(update_state(State,A), Depth-D)
			Variants.append((Est,A))

		if Variants == []: return (-999, None)

		Variants.sort()
		Variants.reverse()

		(Best_est,_) = Variants[0]
		Best_vars = [Variants[i] for i in range(len(Variants)) if Variants[i][0] >= Best_est - self.eps]

		return Best_vars[ int(random()*len(Best_vars)) ]


	def get_min(self,State,Depth):
		"""
		returns the worst move and its Estimation (Est, Move) from the State
		"""
		Actions = get_legal_actions(State)
		if Actions == []: return (999,None)
			
		if Depth <= 0 and Actions[0][1] == '->':
			return (self.get_estimation(State),None)

		Variants = []
		if len(Actions) <= 2: D = self.D1
		elif len(Actions) <= 4: D = self.D2
		#elif len(Actions) > 10: D = 1
		else: D = self.D3 

		for A in Actions:
			(Est,_) = self.get_max(update_state(State,A),Depth-D)
			Variants.append((Est,A))

		if Variants == []: return (999, None)

		Variants.sort()
		
		(Worst_est,_) = Variants[0]
		Worst_vars = [Variants[i] for i in range(len(Variants)) if Variants[i][0] <= Worst_est + self.eps]

		return Worst_vars[ int(random()*len(Worst_vars)) ]


	def get_white_first_move(self):
		return [((5, 0), '->', (4, 0)), ((5, 0), '->', (4, 1)), ((5, 1), '->', (4, 1)),
				((5, 1), '->', (4, 2)), ((5, 2), '->', (4, 2)), ((5, 2), '->', (4, 3)),
				((5, 3), '->', (4, 3))][self.move1]

	def get_black_first_move(self,w_men):
		[J1] = [man[1] for man in w_men if man[0]==4]
		[J0] = [j for j in range(4) if not (j in [man[1] for man in w_men if man[0]==5]) ]
		w_move_index = J0+J1
		b_move_index = self.move2[w_move_index]
		
		return [((2,0), '->', (3,0)), ((2,1), '->', (3,0)), ((2,1), '->', (3,1)),
				((2,2), '->', (3,1)), ((2,2), '->', (3,2)), ((2,3), '->', (3,2)),
				((2,3), '->', (3,3))][b_move_index]


	def get_estimation(self,State):
		(_,Who,Ws,Bs,Es,Wk,Bk) = State
		w_attack = max([ 1/(1+M[0]) for M in Ws]+[0])
		b_attack = max([ 1/(8-M[0]) for M in Bs]+[0])

		w_defense = sum([ 1/((8-M[0])**2+abs(1.2-M[1])) for M in Ws]+[0])
		b_defense = sum([ 1/((1+M[0])**2+abs(1.8-M[1])) for M in Bs]+[0])

		#print "attack: %f, defense: %f" %(self.attack,self.defense)
		#print "value_kings:", self.value_kings(Who,len(Wk),len(Bk))

		return (len(Ws)-len(Bs)) + self.value_kings(Who,len(Wk),len(Bk)) + \
				self.attack * (w_attack - b_attack) + self.defense * (w_defense - b_defense)


	def value_kings(self,Who,w_kings,b_kings):
		if Who == 'w':
			return value_kings(w_kings,self.next_king)*self.king2man_own \
			- value_kings(b_kings,self.next_king)*self.king2man_opp
		else:
			return value_kings(b_kings,self.next_king)*self.king2man_own \
			- value_kings(w_kings,self.next_king)*self.king2man_opp



def value_kings(N,alfa):
	if N == 0:
		return 0
	else:
		return 1 + alfa * value_kings(N-1,alfa)
	


