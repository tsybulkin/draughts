#
# Draughts game
# started September 2014
#

import sys
import os.path

from draughts import main



def debut(data_file, N, Agent1, Agent2):
	Debut_score = read_debut_data(data_file)
	#print Debut_score
	
	for N in range(N):
		(P,Q,Moves) = main(Agent1,Agent2,0)
		Move1 = str(Moves[0][1]).strip()
		Move2 = str(Moves[1][1]).strip()
		Nbr, Score = Debut_score.get((Move1,Move2), (0,0))
		Debut_score[(Move1,Move2)] = (Nbr+1, ((P-Q)+Nbr*Score)/(Nbr+1.0) )
		print 'Game (%i) - Score: %i:%i' % (N+1,P,Q)
		
	write_debut_data(data_file,Debut_score)


def read_debut_data(data_file):
	Debut_score = {}

	if os.path.isfile(data_file):
		f = open(data_file, 'r')
		for line in f:
			S = line.split(";")
			W_move = S[0]
			B_move = S[1]
			Nbr = int(S[2])
			Score = float(S[3])
			Debut_score[(W_move.strip(),B_move.strip())] = (Nbr,Score)

		f.close()
	
	return Debut_score


def write_debut_data(data_file, Debut_score):
	f = open(data_file,'w')
	Keys = Debut_score.keys()
	Keys.sort()
	for key in Keys:
		Nbr, Score = Debut_score[key]
		line = key[0] + '; ' + key[1] + '; ' +  str(Nbr) + '; ' + str(Score) + '\n'
		f.write(line)
	f.close()




if __name__ == '__main__':
	A = sys.argv
	Avail_agents = ['random','egghead','human', 'minimax2']

	if len(A) != 5: 
		print "Usage: python debut.py <data_file> <number of games> <Agent1> <Agent2>"
		print "Available agents:"
		for A in Avail_agents: print " ", A
	elif all( [A[i] in Avail_agents for i in [3,4] ]) and A[2].isdigit():
		debut(A[1],int(A[2]),A[3],A[4])
	else: 
		print "Unknown Agent name or number of games"
		print "Usage: python debut.py <data_file> <number of games> <Agent1> <Agent2>"
		print "Available agents:"
		for A in Avail_agents: print " ", A
