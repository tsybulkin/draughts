#
# Draughts game
# started September 2014
#

import sys

from moves import *
from state import *
from simple_agents import *
from egghead import egghead



def main(Agent1,Agent2,Verb=10):	
	agent = {}
	if Agent1 == 'random':
		agent['w'] = Agent('Random')
		agent['w'].get_move = choose_randomly

	elif Agent1 == 'human':
		agent['w'] = Agent("Human")
		agent['w'].get_move = choose_manually
	
	elif Agent1 == 'minimax2':
		agent['w'] = Agent("MiniMax2")
		agent['w'].get_move = minimax2

	elif Agent1 == 'egghead':
		agent['w'] = Agent("EggHead")
		agent['w'].get_move = egghead

	else:
		raise

	if Agent2 == 'random':
		agent['b'] = Agent('Random')
		agent['b'].get_move = choose_randomly

	elif Agent2 == 'human':
		agent['b'] = Agent('Human')
		agent['b'].get_move = choose_manually

	elif Agent2 == 'minimax2':
		agent['b'] = Agent("Minimax2")
		agent['b'].get_move = minimax2

	elif Agent2 == 'egghead':
		agent['b'] = Agent("EggHead")
		agent['b'].get_move = egghead

	else:
		raise

	Time = 60
	return run_game(agent, Time, Verb)
	



def run_game(agent,TimeBudget, Verb):
	## Initial State
	
	Whites = [ (i,j) for i in range(5,8) for j in range(4) ]
	Blacks = [ (i,j) for i in range(3) for j in range(4) ]
	Empties = [ (i,j) for i in range(3,5) for j in range(4) ]
	State = (1,'w', Whites,Blacks,Empties,[],[] )
	Actions = get_legal_actions(State)
	N = 1
	if Verb > 5: print_board(State)
	T="Whites"
	Turn = 'w'
	Time = {'w':0.0,'b':0.0}
	All_moves = []

	while len(Actions) != 0:
		if Verb>5: print "\n Turn (%i):" % int(N),agent[Turn].name, "(",T,")"
		(Action,T) = agent[Turn].get_move(State)
		All_moves.append((Turn,Action))
		Time[Turn] += T
		if Time[Turn] > TimeBudget: 
			if Turn == 'w': 
				print "Time is over. Black won"
				return (0,1,All_moves)
			else: 
				print "Time is over. White won"
				return (1,0,All_moves) 

		if Verb>5: 
			print "Available moves:"
			for Move in Actions:
				for s in Move:
					print str(s),
				print ""
			print "Move:", Action

		State = update_state(State,Action)
		if Verb >5: print_board(State)
		(N,Turn,_,_,_,Wk,Bk) = State
		if Turn == 'w': T='Whites'
		else: T="Blacks"
		
		Actions = get_legal_actions(State)

		if len(Bk)!=0 and len(Wk)!=0 and N > 35 and abs(len(Wk)-len(Bk)) < 3:
			print "Draw. Time: White:%isec, Black:%isec" % (Time['w'],Time['b'])
			return (0.5, 0.5, All_moves)
		elif N > 47 : 
			if Verb>2: 
				print "Draw. Time: White:%isec, Black:%isec" % (Time['w'],Time['b'])
				print "First moves:\nWhite: %s\nBlack: %s" % (All_moves[0][1],All_moves[1][1])
			return (0.5,0.5,All_moves)

	if State[1] == 'w':
		if Verb>2: 
			print "Black won. Time: White:%isec, Black:%isec" % (Time['w'],Time['b'])
			print "First moves:\nWhite: %s\nBlack: %s" % (All_moves[0][1],All_moves[1][1])
		return (0,1,All_moves)
	else:
		if Verb>2: 
			print "White won. Time: White:%isec, Black:%isec" % (Time['w'],Time['b'])
			print "First moves:\nWhite: %s\nBlack: %s" % (All_moves[0][1],All_moves[1][1])
		return (1,0,All_moves)	
	
	




def print_board(State):
	(N,Who,Ws,Bs,Es,Wk,Bk) = State
	Ls = [ (Man,'w') for Man in Ws ] + [ (Man,'b') for Man in Bs ] + [ (Man,'_') for Man in Es ] + \
		[ (Man,'B') for Man in Bk ] + [ (Man,'W') for Man in Wk ]
	Ls.sort()
	S = [ Sy for (Man,Sy) in Ls]		
	
	print '     '+'_ '*7, "\t Turn: %i" % N
	for i in range(7,-1,-1):
		if i%2 == 0: print "%i"%i,
		else: print "%i  "%i,
		print '|'+S[4*i]+'|_|'+ S[4*i+1]+'|_|'+S[4*i+2]+'|_|'+ S[4*i+3]+'|'
	print "    0   1   2   3 "




if __name__ == '__main__':
	A = sys.argv
	Avail_agents = ['random','egghead','human', 'minimax2']

	if len(A) != 3: 
		print "Usage: python draughts.py <Agent1> <Agent2>"
		print "Available agents:"
		for A in Avail_agents: print " ", A
	elif all( [A[i] in Avail_agents for i in [1,2] ]):
		main(A[1],A[2])
	else: 
		print "Unknown Agent name"
		print "Usage: python draughts.py <Agent1> <Agent2>"
		print "Available agents:"
		for A in Avail_agents: print " ", A
