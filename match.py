#
# Draughts game
# started September 2014
#

import sys

#from moves import *
#from state import *
#from simple_agents import *
#from egghead import egghead

from draughts import main


def match(N, Agent1, Agent2):
	Score = (0,0)
	for N in range(N):
		(P,Q,_) = main(Agent1,Agent2,3)
		Score = (Score[0]+P, Score[1]+Q) 
		#print Score
	print "The score of the match:\nWhite: %g\nBlack: %g" %Score


if __name__ == '__main__':
	A = sys.argv
	Avail_agents = ['random','egghead','human', 'minimax2']

	if len(A) != 4: 
		print "Usage: python match.py <number of games> <Agent1> <Agent2>"
		print "Available agents:"
		for A in Avail_agents: print " ", A
	elif all( [A[i] in Avail_agents for i in [2,3] ]) and A[1].isdigit():
		match(int(A[1]),A[2],A[3])
	else: 
		print "Unknown Agent name or number of games"
		print "Usage: python match.py <number of games> <Agent1> <Agent2>"
		print "Available agents:"
		for A in Avail_agents: print " ", A
